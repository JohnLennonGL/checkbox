package com.example.checkbox;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private CheckBox mCheckGato;
    private CheckBox mCheckCachorro;
    private CheckBox mCheckPassaro;

    private TextView mResultado;
    private Button mBotao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        mCheckGato = findViewById(R.id.BoxGatoID);
        mCheckCachorro = findViewById(R.id.BoxCachorroID);
        mCheckPassaro = findViewById(R.id.BoxPassaroID);
        mResultado = findViewById(R.id.ResultadoID);
        mBotao = findViewById(R.id.BotaoID);


        mBotao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ItensSelecionados = "";

                ItensSelecionados += "item: " + mCheckGato.getText() + " - Status: " + mCheckGato.isChecked() + " \n";
                ItensSelecionados += "item: " + mCheckCachorro.getText() + " - Status: " + mCheckCachorro.isChecked() + " \n";
                ItensSelecionados += "item: " + mCheckPassaro.getText() + " - Status: " + mCheckPassaro.isChecked() + " \n";


                        mResultado.setText(ItensSelecionados);
            }
        });



    }
}
